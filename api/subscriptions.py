''' origen de datos para la suscripcion de notificaciones y captura de eventos'''
import json
from ariadne import SubscriptionType

from .store import pubsub
subscription = SubscriptionType()

@subscription.source("updateNotifications")
async def source_message(_, info):
    ''' obtiene las notificaciones almacenadas'''
    async with pubsub.subscribe(channel="notification_room") as subscriber:
        async for event in subscriber:
            message = json.loads(event.message)
            yield message

@subscription.field("updateNotifications")
def resolve_message(event, info):
    ''' maneja los eventos por socket'''
    return event
