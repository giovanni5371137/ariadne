''' guarda archivos enviados por multipart'''
from starlette.responses import PlainTextResponse

async def file_update(request):
    ''' ontiene los archivos del from data'''
    async with request.form() as form:
        if "upload_file" in form:
            filename = form["upload_file"].filename
            contents = form["upload_file"].file.read()
            with open(filename, "wb") as buffer:
                buffer.write(contents)
                buffer.close()
            return PlainTextResponse('File save')
        return PlainTextResponse('File dont save')
