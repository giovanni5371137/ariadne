''' 
    Configuracion de Graphql con ariadne
    asignacion de ruta para subir archivos multipart
    eventos Broadcaster inicio y fin de comunicacion por websocket ASGI
'''
from ariadne import load_schema_from_path,\
    make_executable_schema
from ariadne.asgi import GraphQL
from ariadne.asgi.handlers import GraphQLTransportWSHandler

from starlette.applications import Starlette
from starlette.middleware.cors import CORSMiddleware
from starlette.routing import Route

from .mutations import mutation
from .queries import query
from .subscriptions import subscription
from .store import pubsub
from .upload import file_update


type_defs = load_schema_from_path("api/schema.graphql")
schema = make_executable_schema(type_defs, query, mutation, subscription)
graphql = CORSMiddleware( GraphQL(
    schema=schema,
    debug=True,
    websocket_handler=GraphQLTransportWSHandler(),
) , allow_origins=['*'], allow_methods=("GET", "POST", "OPTIONS"))

app = Starlette(
    debug=True, routes=[ Route('/file/', file_update, methods=['POST']),],
    on_startup=[pubsub.connect],
    on_shutdown=[pubsub.disconnect],
)
